FROM centos:7 as compile-image
RUN yum install -y centos-release-scl.noarch epel-release.noarch && yum update -y && yum install -y python3 gcc python3-devel llvm-toolset-7-clang llvm5.0-devel.x86_64
RUN yum install -y  https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm && yum install -y postgresql12-devel
ENV PATH="$PATH:/usr/pgsql-12/bin"
WORKDIR /home/workdir
RUN python3 -m venv venv
ENV PATH="/home/workdir/venv/bin:$PATH"
ADD ./requirements.txt requirements.txt
RUN python3 -m pip install -r requirements.txt


FROM centos:7 as build-image
RUN yum update -y && yum install -y python3
RUN yum install -y  https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm && yum install -y  postgresql12-libs
WORKDIR /home/workdir
COPY --from=compile-image /home/workdir/venv /home/workdir/venv
ENV PATH="/home/workdir/venv/bin:$PATH"
CMD ["python3", "django_blog/manage.py", "runserver", "0.0.0.0:8000"]
#CMD ["/bin/bash"]
