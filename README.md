## Usage:
```
git clone https://gitlab.com/akovalev-lrd/django_blog.git
cd django_blog
docker-compose --project-directory ./ -f docker/docker-compose.yml build
docker-compose --project-directory ./ -f docker/docker-compose.yml up -d
docker-compose --project-directory ./ -f docker/docker-compose.yml exec web python3 django_blog/manage.py migrate
```
